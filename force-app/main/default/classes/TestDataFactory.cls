public with sharing class TestDataFactory {
    public static List<Account> createAccounts(Integer count) {
        List<Account> accList = new List<Account>();
        Account acc;
        for(Integer i=0; i < count; i++) {
            acc = new Account(Name = 'Test'+count, ShippingStreet = 'TestStreet'+count, ShippingCity = 'TestCity'+count, 
                    ShippingState = 'TestState'+count, ShippingCountry = 'TestCountry'+count, AddressChangeCount__c = 5);
            accList.add(acc);        
        }
        INSERT accList;
        return accList;
    }
}
