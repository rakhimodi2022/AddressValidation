/*
 * Class Name : AccountTriggerHandler
 * Description : Trigger Handle for Account Trigger
 * CreatedBy : Rakhi Modi on 01/10/2022
 */
public with sharing class AccountTriggerHandler {

    /*
     * Method Name : beforeUpdate
     * Description : to do before update operations
     * CreatedBy : Rakhi Modi on 10/01/2022
    */
    public static void beforeUpdate(List<Account> accOldList, Map<Id, Account> accNewMap) {
        updateAccountAddress(accOldList, accNewMap);
    }

    /*
     * Method Name : updateAccountAddress
     * Description : to updateAccount address and show error on count of morethan 5
     * CreatedBy : Rakhi Modi on 10/01/2022
    */
    private static void updateAccountAddress(List<Account> accOldList, Map<Id, Account> accNewMap) {
        Map<Id, Account> accIdMap = new Map<Id, Account>();
        for(Account acc : accOldList) {
            accIdMap.put(acc.Id, acc);
        }

        /*filteredOldAccList = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingCountry, AddressChangeCount__c
                                FROM Account WHERE Id IN :accIdSet WITH SECURITY_ENFORCED];*/

        List<Account> accNewList = new List<Account>();
        for(Account acc : accIdMap.values()) {
            if((!acc.ShippingStreet.equalsIgnoreCase(accNewMap.get(acc.Id).ShippingStreet))
                || (!acc.ShippingCity.equalsIgnoreCase(accNewMap.get(acc.Id).ShippingCity))
                || (!acc.ShippingState.equalsIgnoreCase(accNewMap.get(acc.Id).ShippingState))
                || (!acc.ShippingCountry.equalsIgnoreCase(accNewMap.get(acc.Id).ShippingCountry))) {
                accNewMap.get(acc.Id).AddressChangeCount__c = accNewMap.get(acc.Id).AddressChangeCount__c == null? 0:accNewMap.get(acc.Id).AddressChangeCount__c;
                if(accNewMap.get(acc.Id).AddressChangeCount__c < 5) {
                    accNewMap.get(acc.Id).AddressChangeCount__c = accNewMap.get(acc.Id).AddressChangeCount__c + 1;
                } else {
                    accNewMap.get(acc.Id).addError(System.Label.AccountAddressUpdateLimitError);
                }
                
            }
            accNewList.add(accNewMap.get(acc.Id));
        }
    }
}
