/*
 * Class Name : AccountTriggerHandler_test
 * Description : Test class for Account Trigger and AccountTriggerHandler
 * CreatedBy : Rakhi Modi on 01/10/2022
 */

 @Istest
public with sharing class AccountTriggerHandler_test {
    @isTest static void testUpdateAccountAddressBelow() {
        // Test data setup
        // Create one account by calling a utility method
        List<Account> acctList = TestDataFactory.createAccounts(1);
        acctList.get(0).AddressChangeCount__c = 4;
        acctList.get(0).shippingCountry = 'South Africa';
        // Perform test
        Test.startTest();
        List<Database.SaveResult> updateResults = Database.update(acctList, false);
        Test.stopTest();
        // Verify that the updation should not have been stopped by the trigger
        System.assert(updateResults.get(0).isSuccess(),'Success');
        System.assert(updateResults.get(0).getErrors().size() <= 0, 'no error');
        
    }
    @isTest static void testUpdateAccountAddressAbove() {
        // Test data setup
        // Create one account by calling a utility method
        List<Account> acctList = TestDataFactory.createAccounts(1);
        acctList.get(0).shippingCity = 'Hyderabad';
        // Perform test
        Test.startTest();
        List<Database.SaveResult> updateResults = Database.update(acctList, false);
        Test.stopTest();
        // Verify that the updation should have been stopped by the trigger,
        // so check that we got back an error.
        System.assert(!updateResults.get(0).isSuccess(),'failed');
        System.assert(updateResults.get(0).getErrors().size() > 0, 'got error');
        System.assertEquals('You have reached the update Address limit. Please contact your administrator',
                            updateResults.get(0).getErrors()[0].getMessage(), 'You have reached the update Address limit.');
    }
}